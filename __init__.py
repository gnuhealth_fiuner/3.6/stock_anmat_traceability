#  -*- coding: utf-8 -*-
# This file is part stock_anmat_traceability module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from .anmat import *
from .party import *
from .product import *
from .lot import *
from .stock import *

from .report import *

from .wizard import *




def register():
    Pool.register(
        AnmatConfig,
        AgentType,
        EventType,
        Event,
        AnmatLocationConfig,
        AnmatTraceTransaction,
        Party,
        Address,
        Template,
        Product,
        Unit,
        Move,
        ShipmentIn,
        ShipmentOut,
        ShipmentInReturn,
        ShipmentOutReturn,
        ShipmentInternal,
        Period,
        PeriodCacheUnit,
        Inventory,
        InventoryLine,
        NotConfirmedTransactionStart,
        NotConfirmedTransactionResultLine,
        NotConfirmedTransactionResult,
        NotConfirmedTransactionEmpty,
        NotConfirmedTransactionAlertResult,
        CancelTransactionStart,
        CancelTransactionResult,
        LoadUnitsStart,
        LoadUnitsStartLine,
        PickUnitsStart,
        PickUnitsStartLine,
        ShipmentInConfirmStart,
        ShipmentInConfirmResult,
        ShipmentOutInformStart,
        ShipmentOutInformResult,
        Lot,
        module='stock_anmat_traceability', type_='model')
    Pool.register(
        LotQRReport,
        module='stock_anmat_traceability', type_='report')
    Pool.register(
        NotConfirmedTransaction,
        CancelTransaction,
        LoadUnits,
        PickUnits,
        ShipmentInConfirm,
        ShipmentOutInform,
        module='stock_anmat_traceability', type_='wizard')
