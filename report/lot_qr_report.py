from trytond.report import Report

__all__ = ['LotQRReport'] 

class LotQRReport(Report):
    __name__ = 'stock.lot.qr_report'

    @classmethod
    def get_context(cls, records, data):
        context = super(LotQRReport, cls).get_context(records,data)

        return context
