Trazabilidad de medicamentos de ANMAT
#####################################

Este módulo permite gestionar la trazabilidad de medicamentos e interactuar
con los webservices de ANMAT (Administración Nacional de Medicamentos,
Alimentos y Tecnología Médica), en Argentina.

Funcionalidad
=============

Habiendo configurado los productos trazables, el módulo permite las siguientes
operaciones con los webservices de ANMAT:

  . Informar la entrega de un medicamento a un cliente o paciente

  . Confirmar la recepción de medicamentos por parte de un proveedor

  . Consultar los medicamentos que un proveedor informa como entregados y cuya
  recepción aún no fue confirmada

  . Alertar el error o falta de recepción de un medicamento que fue informado
  por un proveedor

  . Cancelar una transacción que se ha informado previamente a ANMAT


Configuración
=============

Usuarios y modos de uso
-----------------------

Es necesario configurar el usuario y contraseña habilitados para interactuar
con los webservices de ANMAT.

La configuración de este usuario dependerá del modo de uso que se esté
aplicando.

En modo *Pruebas de servicio* (para pruebas generales y desarrollo) se puede
utilizar la siguiente configuración:

::

  Usuario: pruebasws
  Contraseña: Clave1234
  GLN de usuario de Prueba: glnw

Nota: más abajo se dan detalles sobre la configuración del GLN (Número de
Localización Global)

Antes de poder utilizar los webservices para trazabilidad de medicamentos en el
modo definitivo de producción se requiere realizar el *entrenamiento*, cuyo
objetivo es comprobar el funcionamiento del servicio.

Por lo tanto, en modo *Entrenamiento* deberá utilizar el usuario, contraseña
y GLN asignado en la registración en el modo de entrenamiento. Ver:
https://servicios.pami.org.ar/trazamed/registro.tz

Finalmente, habiendo superado el entrenamiento se puede pasar al modo de
*Producción*. Para este entorno de producción es necesario registrarse en:
https://trazabilidad.pami.org.ar/trazamed/registro.tz


Ubicaciones
-----------

Las ubicaciones constituyen los puntos de recepción, almacenamiento y entrega
de productos en el sistema. Por lo tanto, es necesario definir qué transición
de productos, desde una ubicación a otra, constituye determinado evento que
debe ser informado a ANMAT.

Por ejemplo, el traslado de medicamentos desde 'Zona de entrada' a 'Zona de
Almacenamiento', al ingresar mercadería en un remito de proveedor, constituye
un evento de tipo 'RECEPCIÓN DE PRODUCTO DESDE UN ESLABÓN ANTERIOR'

A su vez, la entrega de medicamentos que consiste en trasladar un medicamento
desde la 'Zona de salida' a 'Cliente' resulta un evento de distinto tipo:
'DISTRIBUCIÓN DEL PRODUCTO A UN ESLABÓN POSTERIOR'

En el sistema se pueden definir distintas ubicaciones, para distintos fines.
Por eso es necesario estipular en qué circunstancia se configura un determinado
evento.

Un ejemplo de configuración sería el siguiente:

  . De 'Zona de entrada' a 'Zona de almacenamiento':
  Tipo de evento '2 - RECEPCIÓN DE PRODUCTO DESDE UN ESLABÓN ANTERIOR'

  . De 'Zona de salida' a 'Cliente':
  Tipo de evento '1 - DISTRIBUCIÓN DEL PRODUCTO A UN ESLABÓN POSTERIOR'

  . De 'Zona de salida' a 'Cliente':
  Tipo de evento '8 - DISPENSACIÓN DEL PRODUCTO AL PACIENTE'

  . De 'Zona de almacenamiento' a 'Proveedor':
  Tipo de evento '5 - ENVÍO DE PRODUCTO EN CARÁCTER DE DEVOLUCIÓN'

  . De 'Cliente' a 'Zona de almacenamiento':
  Tipo de evento '6 - RECEPCIÓN DE PRODUCTO EN CARÁCTER DE DEVOLUCIÓN'


Entidades o pacientes
---------------------

En las Entidades se agrega la pestaña *ANMAT*. Allí es necesario indicar el
tipo de agente de que se trata, por ejemplo: Droguería, Entidad asistencial o
Paciente.

En el caso de que se trate de entidades y no de personas, debe indicarse una
dirección o domicilio, con el código de GLN (Número de Localización Global)
que la identifica: https://en.wikipedia.org/wiki/Global_Location_Number


Productos
---------

Por medio de un tilde incorporado al modelo de Productos, se debe indicar si se
trata de un medicamento trazable. En caso afirmativo, se debe indicar el código
GTIN que le corresponde: https://es.wikipedia.org/wiki/Global_Trade_Item_Number

Si bien la trazabilidad determina números de serie para unidades, el
medicamento puede ser *fraccionable*. Es decir que la unidad puede consistir en
componentes fraccionables que se distribuyen de manera parcial. Si este fuera
el caso, debe indicarse que es un medicamento fraccionable, y la cantidad de
las fracciones.


Más información
---------------

Para más información ver: http://www.anmat.gov.ar/trazabilidad/preguntas.asp


Uso y facilidades incorporadas
==============================

La trazabilidad depende del tipo movimiento de stock que se realice con el
medicamento. Por lo tanto, los documentos relacionados con la trazabilidad son:

  . Remitos de proveedor

  . Remitos de devolución a proveedor

  . Remitos a cliente

  . Remitos de devolución de cliente

  . Remitos internos


Estos documentos incorporan, en el caso de que haya unidades trazables
asociadas, una pestaña denominada *Transacciones ANMAT*. Allí puede verse
información de la unidad, el estado y código de transacción asociado, en caso
de que la transacción se encuentre confirmada.

Para circunstancias especiales en que pueda requerirse, estos documentos
incorporan un tilde (*Informar a ANMAT*) que permite determinar que no se
utilicen los webservices de ANMAT. De ese modo es posible operar con unidades
trazables cuando hay que corregir un error o se prefiere interactuar con ANMAT
por medio de la interfaz web, en lugar de con webservices.


Asistente para carga y selección de unidades
--------------------------------------------

En los Remitos de proveedor y de Clientes se incorporó un asistente que permite
capturar y cargar la unidad medicinal por medio de un lector de código de
barras.

El objetivo es agilizar la operatoria de carga y reducir los errores humanos.


Transacciones No Confirmadas
----------------------------

Este asistente muestra una serie de parámetros (los usuales son el GLN o CUFE
de un proveedor, el número de remito, lote o número de serie) a partir de los
cuales se puede consultar si una o más unidades trazables han sido informadas
como entregadas de parte del proveedor.

Esta facilidad existe para el caso en que se carga un Remito de proveedor y no
resulta posible *confirmar* la recepción de las unidades indicadas. La razón de
este problema puede ser que el proveedor aún no ha informado la entrega de los
productos o hubo un error en la carga de las unidades.

En el caso de que se descubra que existen unidades informadas, pero que nunca
fueron entregadas por el proveedor, es posible *alertar* a ANMAT de esta
irregularidad. Para hacerlo basta con tildar la opción de *Alertar a ANMAT* y
ejecutar la opción *Alertar* del asistente.


Cancelación de transacciones
----------------------------

En los remitos, en la pestaña *Transacciones ANMAT*, se listan las
transacciones asociadas al documento. En el caso de transacciones *confirmadas*
se puede intentar la cancelación, con el botón *Cancelar*.

Es posible seleccionar más de una transacción al momento de cancelar.
