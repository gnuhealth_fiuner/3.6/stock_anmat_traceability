# -*- coding: utf-8 -*-
# This file is part stock_anmat_traceability module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import logging
import traceback
import sys
import datetime

from trytond.model import ModelSingleton, ModelView, ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.error import WarningErrorMixin


__all__ = ['AnmatConfig', 'AgentType', 'EventType', 'Event',
    'AnmatLocationConfig', 'AnmatTraceTransaction']
__metaclass__ = PoolMeta

logger = logging.getLogger(__name__)

HAS_TRAZAMED = False
try:
   from trytond.my_libs.pyafipws.trazamed import TrazaMed
   HAS_TRAZAMED = True
except ImportError:
   logger.warning(
       'Unable to import trazamed. ANMAT actions disabled.',
       exc_info=True)


class AnmatConfig(ModelSingleton, ModelSQL, ModelView):
    'Webservice Configuration'
    __name__ = 'anmat.config'

    user = fields.Char('User')
    password = fields.Char('Password')
    mode = fields.Selection([
        ('testing', 'Testing'),
        ('production', 'Production'),
        ], 'Mode', select=True)

    @staticmethod
    def default_mode():
        return 'testing'


class AgentType(ModelSQL, ModelView):
    "Agent Type"
    __name__ = 'anmat.agent.type'

    name = fields.Char('Name', required=True, translate=True)
    code = fields.Char('Code', required=True)


class EventType(ModelSQL, ModelView):
    "Event Type"
    __name__ = 'anmat.event.type'

    name = fields.Char('Name', required=True, translate=True)
    code = fields.Char('Code', required=True)

    def get_rec_name(self, name):
        return self.code + ' - ' + self.name


class Event(ModelSQL, ModelView):
    "Event"
    __name__ = 'anmat.event'

    code = fields.Char('Event ID', required=True, select=True)
    type = fields.Many2One('anmat.event.type', 'Type', required=True)
    notifier = fields.Many2One('anmat.agent.type', 'Notifier')
    origin = fields.Many2One('anmat.agent.type', 'Origin')
    destination = fields.Many2One('anmat.agent.type', 'Destination')

    def get_rec_name(self, name):
        return self.code + ' - ' + self.type.name


class AnmatLocationConfig(ModelSQL, ModelView):
    'Location Configuration'
    __name__ = 'anmat.location.config'

    from_location = fields.Many2One('stock.location', 'From Location',
            ondelete='RESTRICT', select=True, required=True)
    to_location = fields.Many2One('stock.location', 'To Location',
            ondelete='RESTRICT', select=True, required=True)
    event_type = fields.Many2One('anmat.event.type',
        'Event Type', required=True)


class AnmatTraceTransaction(ModelSQL, ModelView):
    "ANMAT Traceability Transaction"
    __name__ = 'anmat.trace.transaction'

    move = fields.Many2One('stock.move', 'Move', readonly=True,
        required=True, ondelete='CASCADE', select=True)
    unit_reference = fields.Function(fields.Char('Unit'), 'get_unit_reference')
    code = fields.Char('Transaction code', readonly=True)
    date = fields.Date('Date', readonly=True)
    event = fields.Many2One('anmat.event', 'Event', readonly=True,
        ondelete='CASCADE', select=True)
    result = fields.Text('Result', readonly=True)
    state = fields.Selection([
        ('pending', 'Pending'),
        ('confirmed', 'Confirmed'),
        ('rejected', 'Rejected'),
        ('canceled', 'Canceled'),
        ], 'State', required=True, readonly=True)

    @classmethod
    def __setup__(cls):
        super(AnmatTraceTransaction, cls).__setup__()
        cls._buttons.update({
                'cancel': {
                    'invisible': ~Eval('state').in_(['confirmed']),
                    },
                })

    @staticmethod
    def default_state():
        return 'pending'

    def get_unit_reference(self, name):
        ref = self.move.product.name
        if self.move.lot:
            ref += ' | L: ' + self.move.lot.number
        if self.move.anmat_unit:
            ref += ' | U: ' + self.move.anmat_unit.serial_number
        return ref

    @classmethod
    @ModelView.button_action(
        'stock_anmat_traceability.wizard_cancel_transaction')
    def cancel(cls, transactions):
        pass


class AnmatTraceWS(WarningErrorMixin):
    "ANMAT Traceability Webservice"
    shipment_types = (
        'stock.shipment.in',
        'stock.shipment.out',
        'stock.shipment.out.return',
        'stock.shipment.in.return',
        'stock.shipment.internal'
        )
    search_params = {
        'p_id_transaccion_global': None,
        'id_agente_informador': None,
        'id_agente_origen': None,
        'id_agente_destino': None,
        'id_medicamento': None,
        'id_evento': None,
        'fecha_desde_op': None,
        'fecha_hasta_op': None,
        'fecha_desde_t': None,
        'fecha_hasta_t': None,
        'fecha_desde_v': None,
        'fecha_hasta_v': None,
        'n_remito': None,
        'n_factura': None,
        'estado': None,
        'lote': None,
        'numero_serial': None,
        }
    send_params = {
        'f_evento': None,
        'h_evento': None,
        'gln_origen': None,
        'gln_destino': None,
        'n_remito': None,
        'n_factura': None,
        'vencimiento': None,
        'gtin': None,
        'lote': None,
        'numero_serial': None,
        'desde_numero_serial': None,
        'hasta_numero_serial': None,
        'cantidad': None,
        # 'codigo_transaccion': None,
        'id_obra_social': None,
        'id_evento': None,
        'cuit_origen': None,
        'cuit_destino': None,
        'apellido': None,
        'nombres': None,
        'tipo_documento': None,
        'n_documento': None,
        'sexo': None,
        'direccion': None,
        'numero': None,
        'piso': None,
        'depto': None,
        'localidad': None,
        'provincia': None,
        'n_postal': None,
        'fecha_nacimiento': None,
        'telefono': None,
        }

    _error_messages = {
        'missing_module': 'Missing PyAfipWS module',
        'missing_ws_configuration': 'Missing Webservice configuration',
        'not_confirmed_transactions_error': 'An error occured while '
        'obtaining not confirmed transactions'
    }

    def __init__(self):
        if not HAS_TRAZAMED:
            self.raise_user_error('missing_module')
        else:
            self.ws = TrazaMed()
            self.ws.Username = "testwservice"
            self.ws.Password = "testwservicepsw"
            self.messages = ''

    def confirm(self, shipment):
        if not shipment:
            return True
        assert shipment.__name__ in self.shipment_types, (
            "'shipment' must be a type of shipment")

        pool = Pool()
        Config = pool.get('anmat.config')
        AnmatTrace = pool.get('anmat.trace.transaction')

        move_shipment = shipment.__name__ + ',' + str(shipment.id)
        pending = AnmatTrace.search([
            ('state', '!=', 'confirmed'),
            ('move.shipment', '=', move_shipment),
            ('move.state', '=', 'done'),
            ])
        if not pending:
            return True

        config = Config(1)
        if not config.user or not config.password:
            self.raise_user_error('missing_ws_configuration')

        self.ws.Conectar()
        params = {
            'id_agente_origen': shipment.supplier.addresses[0].gln_number,
            'id_agente_destino':
                shipment.company.party.addresses[0].gln_number,
            }
        search_params = self.get_search_parameters(**params)
        self.getNotConfirmedTransactions(
            usuario=config.user,
            password=config.password,
            **search_params)

        if self.ws.HayError:
            self.raise_user_error('not_confirmed_transactions_error')

        while self.ws.LeerTransaccion():
            _id_transaccion = self.ws.GetParametro('_id_transaccion')
            _f_operacion = datetime.datetime.now().strftime("%d/%m/%Y")
            lot_number = self.ws.GetParametro('_lote')
            serial_number = self.ws.GetParametro('_numero_serial')
            transaction = AnmatTrace.search([
                ('state', '=', 'pending'),
                ('move.shipment', '=', move_shipment),
                ('move.state', '=', 'done'),
                ('move.lot.number', '=', lot_number),
                ('move.anmat_unit.serial_number', '=', serial_number),
                ], order=[
                ('date', 'ASC'), ('id', 'ASC'),
                ])
            if transaction:
                transaction = AnmatTrace.browse([t.id for t in transaction])
                confirmationOk = False
                try:
                    confirmationOk = self.ws.SendConfirmaTransacc(
                        usuario=config.user,
                        password=config.password,
                        p_ids_transac=_id_transaccion,
                        f_operacion=_f_operacion,
                        )
                except Exception as e:
                    if self.ws.Excepcion:
                        self.messages += self.ws.Excepcion + ' ' + str(e) + \
                            '\n' + self.ws.Traceback
                    else:
                        self.messages += traceback.format_exception_only(
                            sys.exc_info()[0], sys.exc_info()[1])[0]
                else:
                    for err in self.ws.Errores:
                        self.messages += err + '\n'

                result = {}
                if confirmationOk:
                    result['state'] = 'confirmed'
                    result['code'] = self.ws.CodigoTransaccion or \
                        _id_transaccion
                result['result'] = self.messages
                self.messages = ''
                AnmatTrace.write(transaction, result)
        return True

    def send(self, shipment):
        if not shipment:
            return True
        assert shipment.__name__ in self.shipment_types, (
            "'shipment' must be a type of shipment")

        pool = Pool()
        Config = pool.get('anmat.config')
        AnmatTrace = pool.get('anmat.trace.transaction')

        config = Config(1)
        if not config.user or not config.password:
            self.raise_user_error('missing_ws_configuration')

        params_list = self._get_parameters(shipment)
        if not params_list:
            return True
        self.ws.Conectar()
        for params in params_list:
            transactions = params.pop('transactions_ids')
            method = 'SendMedicamentos'
            if 'numero_serial' not in params:
                method = 'SendMedicamentosDHSerie'
            elif 'cantidad' in params:
                method = 'SendMedicamentosFraccion'

            confirmationOk = False
            try:
                confirmationOk = getattr(self.ws, method)(usuario=config.user,
                    password=config.password, **params)
            except Exception as e:
                if self.ws.Excepcion:
                    self.messages += self.ws.Excepcion + ' ' + str(e) + \
                        '\n' + self.ws.Traceback
                else:
                    self.messages += traceback.format_exception_only(
                        sys.exc_info()[0], sys.exc_info()[1])[0]
            else:
                for err in self.ws.Errores:
                    serials = ''
                    if method == 'SendMedicamentosDHSerie':
                        serials = str(params['desde_numero_serial']) + \
                            ' a ' + str(params['hasta_numero_serial'])
                    else:
                        serials = str(params['numero_serial'])
                    self.messages += 'Doc. ' + shipment.rec_name + ': ' + \
                        'Lote ' + params['lote'] + ' : ' + \
                        serials + '  - ' + err + '\n'

            result = {}
            if confirmationOk:
                result['state'] = 'confirmed'
                result['code'] = self.ws.CodigoTransaccion
            result['result'] = self.messages
            self.messages = ''
            transactions = AnmatTrace.browse(transactions)
            AnmatTrace.write(transactions, result)

        return True

    def getNotConfirmedTransactions(self, usuario, password, **params):
        self.ws.GetTransaccionesNoConfirmadas(
            usuario=usuario, password=password, **params
            )

    def get_search_parameters(self, **params):
        if not params:
            return {}
        params = {k: v for k, v in list(params.items()) if v is not None and v != ''}
        for key in self.search_params:
            self.search_params[key] = None
        for key in (p for p in params if p in self.search_params):
            self.search_params[key] = params[key]
        return self.search_params

    def _get_parameters(self, shipment):
        '''
        Get a shipment and returns a list of dictionaries with parameters
        for send() method. It checks for consecutive serials.
        '''
        pool = Pool()
        Lot = pool.get('stock.lot')
        AnmatTrace = pool.get('anmat.trace.transaction')
        if not shipment:
            return []

        move_shipment = shipment.__name__ + ',' + str(shipment.id)
        transactions = AnmatTrace.search([
            ('state', '=', 'pending'),
            ('move.shipment', '=', move_shipment),
            ('move.state', '=', 'done'),
            ], order=[
                ('move.lot.id', 'ASC'),
                ('move.anmat_unit.serial_number', 'ASC'),
            ])

        if not transactions:
            return []
        event_code = transactions[0].event.code
        # grouped_serials is a list of tuples:
        #   [(lot, [serial_start, serial_end], [transactions_ids, ...]), ...]
        grouped_serials = []
        lots = list(set([t.move.lot.id for t in transactions]))
        for l in lots:
            serial_start = None
            serial_end = None
            t_ids = []
            trx_per_lot = [t for t in transactions if t.move.lot.id == l]
            for t in trx_per_lot:
                serial_number = t.move.anmat_unit.serial_number
                if not serial_start:
                    serial_start = serial_number
                    serial_end = serial_number
                    t_ids.append(t.id)
                else:
                    if int(serial_end) + 1 == int(serial_number):
                        serial_end = serial_number
                        t_ids.append(t.id)
                    else:
                        grouped_serials.append(
                            (l, [serial_start, serial_end], t_ids))
                        serial_start = serial_number
                        serial_end = serial_number
                        t_ids = [t.id]
            if serial_start:
                grouped_serials.append(
                    (l, [serial_start, serial_end], t_ids))

        params_list = []
        company = shipment.company.party.addresses[0].gln_number
        destination = shipment.customer.addresses[0].gln_number
        f_evento = shipment.effective_date.strftime('%d/%m/%Y')
        for group in grouped_serials:
            lot = Lot.browse([group[0]])
            transactions_ids = group[2]
            gtin = lot[0].product.anmat_gtin
            lote = lot[0].number
            serials = group[1]
            numero_serial = None
            desde_numero_serial = None
            hasta_numero_serial = None
            if serials[0] == serials[1]:
                numero_serial = int(serials[0])
            else:
                desde_numero_serial = int(serials[0])
                hasta_numero_serial = int(serials[1])
            params = self.send_params.copy()
            params['f_evento'] = f_evento
            params['h_evento'] = '12:00'
            params['gln_origen'] = company
            params['gln_destino'] = destination
            params['gtin'] = gtin
            params['lote'] = lote
            if numero_serial:
                params['numero_serial'] = numero_serial
                trx = AnmatTrace.browse(transactions_ids)
                if trx[0].move.quantity > 1:
                    params['cantidad'] = int(trx[0].move.quantity)
                else:
                    params.pop('cantidad')
                params.pop('desde_numero_serial')
                params.pop('hasta_numero_serial')
            else:
                params['desde_numero_serial'] = desde_numero_serial
                params['hasta_numero_serial'] = hasta_numero_serial
                params.pop('numero_serial')
            params['id_evento'] = event_code
            params['transactions_ids'] = transactions_ids
            params_list.append(params)
        return params_list

    def cancel(self, transactions):
        if not transactions:
            return True
        pool = Pool()
        Config = pool.get('anmat.config')
        AnmatTrace = pool.get('anmat.trace.transaction')

        config = Config(1)
        if not config.user or not config.password:
            self.raise_user_error('missing_ws_configuration')
        self.ws.Conectar()
        for transaction in transactions:
            code_transaction = transaction.code
            gtin = transaction.move.product.anmat_gtin
            lot = transaction.move.lot.number
            serial = transaction.move.anmat_unit.serial_number
            try:
                self.ws.SendCancelacTransaccParcial(
                    usuario=config.user, password=config.password,
                    codigo_transaccion=code_transaction, gtin_medicamento=gtin,
                    numero_serial=serial)
            except Exception as e:
                if self.ws.Excepcion:
                    self.messages += self.ws.Excepcion + ' ' + str(e) + \
                        '\n' + self.ws.Traceback
                else:
                    self.messages += traceback.format_exception_only(
                        sys.exc_info()[0], sys.exc_info()[1])[0]
            else:
                for err in self.ws.Errores:
                    self.messages += 'Transac. ' + code_transaction + ': ' + \
                        'Lote ' + lot + ' - Serial ' + serial + \
                        ' : ' + err + '\n'

            result = {}
            if self.ws.Resultado and not self.ws.Errores:
                result['state'] = 'canceled'
                result['code'] = self.ws.CodigoTransaccion
            result['result'] = 'Cancelación\n' + self.messages
            transactions = AnmatTrace.browse(transactions)
            AnmatTrace.write(transactions, result)

        return True

    def alertTransaction(self, id_transaccion):
        if not id_transaccion:
            return True
        pool = Pool()
        Config = pool.get('anmat.config')

        config = Config(1)
        if not config.user or not config.password:
            self.raise_user_error('missing_ws_configuration')
        self.ws.Conectar()
        try:
            self.ws.SendAlertaTransacc(
                usuario=config.user, password=config.password,
                p_ids_transac_ws=id_transaccion)
        except Exception as e:
            if self.ws.Excepcion:
                self.messages += self.ws.Excepcion + ' ' + str(e) + \
                    '\n' + self.ws.Traceback
            else:
                self.messages += traceback.format_exception_only(
                    sys.exc_info()[0], sys.exc_info()[1])[0]
        else:
            for err in self.ws.Errores:
                self.messages += 'Transac. ' + str(id_transaccion) + ': ' + \
                    err + '\n'

        return True
