# -*- coding: utf-8 -*-
# This file is part stock_anmat_traceability module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pyson import Not, Eval, Bool, Equal
from trytond.pool import PoolMeta

__all__ = ['Template','Product']
__metaclass__ = PoolMeta

class Template(metaclass = PoolMeta):
    'Template'
    __name__ = 'product.template'

    @staticmethod
    def default_shelf_life_state():
        return 'optional'

    @staticmethod
    def default_expiration_state():
        return 'optional'


class Product (metaclass = PoolMeta):
    'Product'
    __name__ = 'product.product'

    gtin_type = fields.Selection([
        (None, ''),
        ('standard','Standard'),
        ('custom','Custom'),
        ],'GTIN Type',sort=False, required=True)
    custom_gtin = fields.Char('Custom GTIN',
        states={
            'invisible': ~Equal(Eval('gtin_type'),'custom'),
        })
    anmat_traceable = fields.Boolean('Is traceable',
        help='Check if the product is traceable by ANMAT')
    anmat_gtin_indicator_digits = fields.Char(
        'GTIN Indicator Digits',
        states={
            'invisible': ~Equal(Eval('gtin_type'),'standard'),
            },
        help='Two or more digits GTIN indicator\n.'\
                '(prefix to GTIN)',)
    anmat_gtin = fields.Char('GTIN',
        states={
            'required': Bool(Eval('anmat_traceable')),
            'invisible': ~Equal(Eval('gtin_type'),'standard'),
            },
        depends=['anmat_traceable'],
        help='GTIN Number')
    anmat_fractionable = fields.Boolean('Is fractionable',
        help='Check if the product is fractionable')
    anmat_fractions = fields.Integer('Fractions',
        states={
            'required': Bool(Eval('anmat_traceable')),
            'readonly': Not(Bool(Eval('anmat_fractionable'))),
            },
        depends=['anmat_traceable', 'anmat_fractionable'],
        help='If fractionable, fractions must be greater than 1')
    qty_in = fields.Integer(
        "Cantidades de entrada por defecto",
        help=u"Cantidades contables en los movimientos de "\
            u"albaranes (remitos) desde proveedor",
        states = {
            'readonly': Bool(Eval('anmat_fractionable'))
            },
        )
    qty_out = fields.Integer(
        "Cantidades de salida por defecto",
        help= u"Cantidades salida en los movimientos \n de "\
            u"albaranes (remitos) hacia cliente",
        states = {
            'readonly': Bool(Eval('anmat_fractionable'))
            },
        )

    @staticmethod
    def default_tin_type():
        return 'standard'

    @staticmethod
    def default_anmat_traceable():
        return False

    @staticmethod
    def default_anmat_fractionable():
        return False

    @staticmethod
    def default_anmat_fractions():
        return 1
    
    @staticmethod
    def default_qty_in():
        return 1

    @staticmethod
    def default_qty_out():
        return 1

    @staticmethod
    def default_anmat_gtin_indicator_digits():
        return '01'

    @fields.depends('anmat_fractionable')
    def on_change_anmat_fractionable(self):
        if self.anmat_fractionable:
            self.qty_out = 1
            self.qty_in = 1

