# -*- coding: utf-8 -*-
#This file is part stock_anmat_traceability module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool

from ..anmat import AnmatTraceWS

__all__ = ['NotConfirmedTransactionStart', 'NotConfirmedTransactionResultLine',
    'NotConfirmedTransactionResult', 'NotConfirmedTransactionEmpty',
    'NotConfirmedTransactionAlertResult', 'NotConfirmedTransaction']


class NotConfirmedTransactionStart(ModelView):
    'Not Confirmed Transaction Start'
    __name__ = 'anmat.not_confirmed_transaction.start'

    p_id_transaccion_global = fields.Integer('Global Transaction ID')
    id_agente_informador = fields.Char('Informer Agent GLN/CUFE')
    id_agente_origen = fields.Char('Origin Agent GLN/CUFE')
    id_agente_destino = fields.Char('Destination Agent GLN/CUFE')
    id_medicamento = fields.Char('Product GTIN')
    id_evento = fields.Integer('Event ID')
    fecha_desde_op = fields.Date('Operation Date From')
    fecha_hasta_op = fields.Date('Operation Date To')
    fecha_desde_t = fields.Date('Transaction Date From')
    fecha_hasta_t = fields.Date('Transaction Date To')
    fecha_desde_v = fields.Date('Due Date From')
    fecha_hasta_v = fields.Date('Due Date To')
    n_remito = fields.Char('Shipment Number')
    n_factura = fields.Char('Invoice Number')
    estado = fields.Selection([
        ('-1', 'Informed'),
        ('1', 'Alerted'),
        ], 'State')
    lote = fields.Char('Lot')
    numero_serial = fields.Char('Serial Number')


class NotConfirmedTransactionResultLine(ModelView):
    'Not Confirmed Transaction List'
    __name__ = 'anmat.not_confirmed_transaction.result.line'

    result = fields.Many2One('anmat.not_confirmed_transaction.result',
        'Result', required=True, select=True)
    id_transaccion = fields.Char('Transaction ID', readonly=True)
    id_transaccion_global = fields.Char('Global Transaction ID', readonly=True)
    f_evento = fields.Char('Event Date', readonly=True)
    f_transaccion = fields.Char('Transaction Date', readonly=True)
    gtin = fields.Char('GTIN', readonly=True)
    lote = fields.Char('Lot', readonly=True)
    numero_serial = fields.Char('Serial Number', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    id_evento = fields.Char('Event ID', readonly=True)
    d_evento = fields.Char('Event Description', readonly=True)
    gln_origen = fields.Char('Origin GLN', readonly=True)
    razon_social_origen = fields.Char('Origin Name', readonly=True)
    gln_destino = fields.Char('Destination GLN', readonly=True)
    razon_social_destino = fields.Char('Destination Name', readonly=True)
    n_remito = fields.Char('Shipment Number', readonly=True)
    n_factura = fields.Char('Invoice Number', readonly=True)
    vencimiento = fields.Char('Due Date', readonly=True)
    alertar = fields.Boolean('Alert ANMAT', help='Check to alert ANMAT')
    estado = fields.Selection([
        ('informado', 'Informado'),
        ('alertado', 'Alertado'),
        ], 'State', readonly=True)


class NotConfirmedTransactionResult(ModelView):
    'Not Confirmed Transaction Result'
    __name__ = 'anmat.not_confirmed_transaction.result'

    transactions = fields.One2Many(
        'anmat.not_confirmed_transaction.result.line',
        'result', 'Transactions')


class NotConfirmedTransactionEmpty(ModelView):
    'Not Confirmed Transaction Empty'
    __name__ = 'anmat.not_confirmed_transaction.empty'


class NotConfirmedTransactionAlertResult(ModelView):
    'Not Confirmed Transaction Alert Result'
    __name__ = 'anmat.not_confirmed_transaction.alert_result'

    info = fields.Text('Info', readonly=True)


class NotConfirmedTransaction(Wizard):
    'Not Confirmed Transaction'
    __name__ = 'anmat.not_confirmed_transaction'

    start = StateView('anmat.not_confirmed_transaction.start',
        'stock_anmat_traceability.not_confirmed_transaction_start_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Search', 'search', 'tryton-ok', default=True),
        ])
    search = StateTransition()
    result = StateView('anmat.not_confirmed_transaction.result',
        'stock_anmat_traceability.not_confirmed_transaction_result_view', [
            Button('Try again', 'start', 'tryton-go-next'),
            Button('Alert', 'alert', 'tryton-go-next'),
            Button('Close', 'end', 'tryton-close', default=True),
            ])
    empty = StateView('anmat.not_confirmed_transaction.empty',
        'stock_anmat_traceability.not_confirmed_transaction_empty_view', [
            Button('Try again', 'start', 'tryton-go-next'),
            Button('Close', 'end', 'tryton-close', default=True),
            ])
    alert = StateTransition()
    alert_result = StateView('anmat.not_confirmed_transaction.alert_result',
        'stock_anmat_traceability.not_confirmed_transaction_alert_result_view',
        [
            Button('Try again', 'start', 'tryton-go-next'),
            Button('Close', 'end', 'tryton-close', default=True),
        ])
    records_list = []

    @classmethod
    def __setup__(cls):
        super(NotConfirmedTransaction, cls).__setup__()
        cls._error_messages.update({
            'missing_module': 'Missing PyAfipWS module',
            'missing_ws_configuration': 'Missing Webservice configuration',
            'not_confirmed_transactions_error': 'An error occured while '
            'obtaining not confirmed transactions',
            'alert_transactions_error': 'An error occured while trying to '
            'alert transactions',
        })

    def default_start(self, fields):
        return {
            'estado': '-1',
            }

    def transition_search(self):
        pool = Pool()
        Config = pool.get('anmat.config')
        TransactionParams = pool.get('anmat.not_confirmed_transaction.start')
        anmatWS = AnmatTraceWS()

        config = Config(1)
        if not config.user or not config.password:
            self.raise_user_error('missing_ws_configuration')

        line_fields = []
        for f in TransactionParams.fields_get():
            if f not in ('create_uid', 'create_date',
                    'write_uid', 'write_date', 'id', 'rec_name'):
                line_fields.append(f)
        anmatWS.ws.Conectar()
        params = {}
        for field in line_fields:
            if field in ('fecha_desde_op', 'fecha_hasta_op', 'fecha_desde_t',
                    'fecha_hasta_t', 'fecha_desde_v', 'fecha_hasta_v'):
                value = getattr(self.start, field)
                params[field] = value.strftime('%d/%m/%Y') if value else None
            elif field in ('p_id_transaccion_global', 'id_evento', 'estado'):
                if getattr(self.start, field):
                    params[field] = int(getattr(self.start, field))
            else:
                params[field] = getattr(self.start, field)

        search_params = anmatWS.get_search_parameters(**params)
        anmatWS.getNotConfirmedTransactions(
            usuario=config.user,
            password=config.password,
            **search_params)

        if anmatWS.ws.HayError:
            self.raise_user_error('not_confirmed_transactions_error')

        records_list = []
        while anmatWS.ws.LeerTransaccion():
            data = {
                'id_transaccion': anmatWS.ws.GetParametro('_id_transaccion'),
                'id_transaccion_global': anmatWS.ws.GetParametro(
                    '_id_transaccion_global'),
                'f_evento': anmatWS.ws.GetParametro('_f_evento'),
                'f_transaccion': anmatWS.ws.GetParametro('_f_transaccion'),
                'gtin': anmatWS.ws.GetParametro('_gtin'),
                'lote': anmatWS.ws.GetParametro('_lote'),
                'numero_serial': anmatWS.ws.GetParametro('_numero_serial'),
                'nombre': anmatWS.ws.GetParametro('_nombre'),
                'id_evento': anmatWS.ws.GetParametro('_id_evento'),
                'd_evento': anmatWS.ws.GetParametro('_d_evento'),
                'gln_origen': anmatWS.ws.GetParametro('_gln_origen'),
                'razon_social_origen': anmatWS.ws.GetParametro(
                    '_razon_social_origen'),
                'gln_destino': anmatWS.ws.GetParametro('_gln_destino'),
                'razon_social_destino': anmatWS.ws.GetParametro(
                    '_razon_social_destino'),
                'n_remito': anmatWS.ws.GetParametro('_n_remito'),
                'n_factura': anmatWS.ws.GetParametro('_n_factura'),
                'vencimiento': anmatWS.ws.GetParametro('_vencimiento'),
                'alertar': False,
                'estado':
                    'informado' if self.start.estado == '-1' else 'alertado',
                }
            records_list.append(data)

        if records_list:
            self.records_list = records_list
            return 'result'
        return 'empty'

    def default_result(self, fields):
        return {'transactions': self.records_list}

    def transition_alert(self):
        message = ''
        alert_list = [t.id_transaccion for t in self.result.transactions
            if t.alertar is True and t.estado == 'informado']
        if not alert_list:
            message = 'No hay transacciones para alertar.'
        else:
            anmatWS = AnmatTraceWS()
            message = 'Transacciones alertadas:'
            for id_transaction in alert_list:
                anmatWS.alertTransaction(int(id_transaction))
                if anmatWS.ws.HayError:
                    self.raise_user_error('alert_transactions_error')
                message += '\n' + id_transaction

        self.alert_result.info = message
        return 'alert_result'

    def default_alert_result(self, fields):
        return {
            'info': self.alert_result.info,
            }
