# -*- coding: utf-8 -*-
#This file is part stock_anmat_traceability module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from ..anmat import AnmatTraceWS

from trytond.model import ModelView, fields
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction

__all__ = ['CancelTransactionStart', 'CancelTransactionResult',
    'CancelTransaction']


class CancelTransactionStart(ModelView):
    'Cancel Transaction Start'
    __name__ = 'anmat.cancel_transaction.start'

    info = fields.Text('Info', readonly=True)


class CancelTransactionResult(ModelView):
    'Cancel Transaction Result'
    __name__ = 'anmat.cancel_transaction.result'

    info = fields.Text('Info', readonly=True)


class CancelTransaction(Wizard):
    'Cancel Transaction'
    __name__ = 'anmat.cancel_transaction'

    start_state = 'start'
    start = StateView('anmat.cancel_transaction.start',
        'stock_anmat_traceability.cancel_transaction_start_view', [
            Button('Cancel Transaction', 'cancel_transaction', 'tryton-ok',
                default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    cancel_transaction = StateTransition()
    result = StateView('anmat.cancel_transaction.result',
        'stock_anmat_traceability.cancel_transaction_result_view', [
            Button('Ok', 'end', 'tryton-ok', True),
            ])

    @classmethod
    def __setup__(cls):
        super(CancelTransaction, cls).__setup__()
        cls._error_messages.update({
                'service_unavailable': ('The ANMAT service is unavailable, '
                    'try again later.'),
                })

    @staticmethod
    def default_start(fields):
        return {
            'info': 'ATENCIÓN: Se cancelarán transacciones de ANMAT',
            }

    def transition_cancel_transaction(self):
        AnmatTrace = Pool().get('anmat.trace.transaction')

        transactions = AnmatTrace.browse(
            Transaction().context.get('active_ids'))
        ws = AnmatTraceWS()
        if transactions:
            try:
                ws.cancel(transactions)
            except Exception as e:
                if hasattr(e, 'faultstring') \
                        and hasattr(e.faultstring, 'find'):
                    if e.faultstring.find('SERVICE_UNAVAILABLE') \
                            or e.faultstring.find('MS_UNAVAILABLE') \
                            or e.faultstring.find('TIMEOUT') \
                            or e.faultstring.find('SERVER_BUSY'):
                        self.raise_user_error('service_unavailable')
                raise
            self.result.info = ws.messages
        return 'result'

    def default_result(self, fields):
        return {
            'info': self.result.info,
            }
