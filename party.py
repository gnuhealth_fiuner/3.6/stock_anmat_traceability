# -*- coding: utf-8 -*-
# This file is part stock_anmat_traceability module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Party', 'Address']
__metaclass__ = PoolMeta


class Party (metaclass = PoolMeta):
    'Party'
    __name__ = 'party.party'

    anmat_agent_type = fields.Many2One('anmat.agent.type', 'Agent Type')


class Address(metaclass = PoolMeta):
    'Address'
    __name__ = 'party.address'

    gln_number = fields.Char('GLN Number')
