import qrcode
import barcode
import io

from trytond.model import fields
from trytond.pool import PoolMeta, Pool

__all__ = ['Lot']


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'

    qr_info = fields.Function(
        fields.Binary('QR Info'), 'on_change_with_qr_info')

    @fields.depends('product','number','expiration_date')
    def on_change_with_qr_info(self, name = None):
        if self.product and self.product.gtin_type == 'custom':
            custom_gtin = self.product.custom_gtin
            expiration_date = self.expiration_date and self.expiration_date.strftime("%d%m%Y") or ''
            lot_number = self.number or ''
            qr_string = 'custom'+custom_gtin+'expdate'+ expiration_date+'lotnumber'+lot_number

            qr_image = qrcode.make(qr_string)

            # Make a PNG image from PIL without the need to create a temp file

            holder = io.BytesIO()
            qr_image.save(holder)
            qr_png = holder.getvalue()
            holder.close()
            return bytearray(qr_png)
        return None


